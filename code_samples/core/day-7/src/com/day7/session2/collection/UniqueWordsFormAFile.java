package com.day7.session2.collection;
import java.util.*;
import java.io.*;
public class UniqueWordsFormAFile {

	public static void main(String[] args) {
		Set<String> set=new TreeSet<String>();
		processUniqueWords(set);
		System.out.println("print all unique words in sorted order");
		printUniqueWords(set);
	}

	private static void printUniqueWords(Set<String> set) {
		for(String word: set) {
			System.out.println(word);
		}
	}

	private static void processUniqueWords(Set<String> set) {
		try {
		BufferedReader br=new BufferedReader(new FileReader(new File("data.txt")));
		String line=null;
		while((line=br.readLine())!=null) {
			String tokens[]=line.split(" ");
			for(String token : tokens) {
				set.add(token.toLowerCase());
			}
		}
		}catch(FileNotFoundException ex) {
			ex.printStackTrace();
		}catch(IOException ex) {
			ex.printStackTrace();
		}
	}
}
