package com.day7.session1.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class DemoCharVsByteSteam {

	public static void main(String[] args) {
		try {
			FileReader fr=new FileReader(new File("ali.jpeg"));
			FileWriter fw=new FileWriter(new File("ali3.jpeg"));
//			FileInputStream fr=new FileInputStream(new File("ali.jpeg"));
//			FileOutputStream fw=new FileOutputStream(new File("ali_copy2.jpeg"));
			int i=0;
			while((i=fr.read())!=-1) {
				fw.write(i);
			}
			fw.flush();
			fr.close();
			fw.close();
		} catch (FileNotFoundException e) {
			System.out.println("file is not found");
		} catch (IOException e) {
			System.out.println("some io ex");
		}
	}
}
