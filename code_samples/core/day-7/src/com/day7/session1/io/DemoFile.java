package com.day7.session1.io;

import java.io.File;
import java.io.FileFilter;

class MyFileFilter implements FileFilter{
	@Override
	public boolean accept(File pathname) {
		return pathname.isHidden();
	}
	
}
public class DemoFile {

	public static void main(String[] args) {
		//We want to print all the files from a directory?
		
//		File dir=new File("/opt/sts_eclipse");
//		if(dir.isDirectory()) {
//			//now i know it is a dir i want to print all files names
//			File []files=dir.listFiles();
//			//now i want it iterate it so that i can print them
//			for(File file: files) {
//				System.out.println(file.getName()+" : "+ file.getPath());
//			}
//		}
		
		//concept of file filters? only hiddens files from a dir
		
		File dir=new File("/opt/sts_eclipse");
		if(dir.isDirectory()) {
			//now i know it is a dir i want to print all files names
			File []files=dir.listFiles(new MyFileFilter());
			//now i want it iterate it so that i can print them
			for(File file: files) {
				System.out.println(file.getName()+" : "+ file.getPath());
			}
		}
		
	}
}
