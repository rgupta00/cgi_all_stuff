package com.day7.session1.io;

import java.io.Serializable;

public class Account implements Serializable{
	private int id;
	private String name;
	private double balance;
	
	private transient String password;// hey java pl ingonre this filed during serilization
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Account(int id, String name, double balance) {
		
		this.id = id;
		this.name = name;
		this.balance = balance;
	}
	public void deposit(double amount) {
		double temp=balance+ amount;
		
		balance=temp;
	}
	public void withdraw(double amount) {
		double temp=balance-amount;
		
		balance=temp;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	@Override
	public String toString() {
		return "Account [id=" + id + ", name=" + name + ", balance=" + balance + ", password=" + password + "]";
	}
	

}
