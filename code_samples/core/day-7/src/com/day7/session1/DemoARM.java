package com.day7.session1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class DemoARM {
	
	public static void main(String[] args) {
		
		try(Scanner scanner=new Scanner(System.in) ){
			System.out.println("PE a no");
			int value=scanner.nextInt();
			System.out.println(value);
		}
		
//		Scanner scanner=null;
//		try{
//			scanner=new Scanner(System.in);
//		}finally {
//			if(scanner!=null) {
//				scanner.close();
//			}
//		}

		
		//ARM syntex : as soon as try block is finished pl close the resouce urself
//		try(BufferedReader  br=new BufferedReader(new FileReader("data.txt"))){
//			
//		} catch (FileNotFoundException e) {
//				System.out.println("file is not found");
//		} catch (IOException e) {
//			System.out.println("some io exception");
//		}
		
		
		
//		BufferedReader br=null;
//		try {
//			 br=new BufferedReader(new FileReader(new File("data.txt")));
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		}finally {
//			if(br!=null) {
//				try {
//					br.close();
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//			}
//		}
		
//		Scanner scanner=new Scanner(System.in);
//		
//		scanner.close();
	}

}
