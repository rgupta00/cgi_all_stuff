package com.day7.session1.collection;

import java.util.Comparator;


class SortAsPerSalaryThenAsPerName implements Comparator<Employee>{
	@Override
	public int compare(Employee o1, Employee o2) {
		int val= Double.compare(o2.getSalary(), o1.getSalary());
		if(val!=0) {
			return val;
		}else {
			return o1.getName().compareTo(o2.getName());
		}
	}
}

class SalarySorter implements Comparator<Employee>{

	@Override
	public int compare(Employee o1, Employee o2) {
		return Double.compare(o2.getSalary(), o1.getSalary());
	}
	
}
public class Employee implements Comparable<Employee>{
	private int id;
	private String name;
	private String dept;
	private double salary;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}

	
	
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public Employee(int id, String name, String dept, double salary) {
		super();
		this.id = id;
		this.name = name;
		this.dept = dept;
		this.salary = salary;
	}
	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", dept=" + dept + ", salary=" + salary + "]";
	}

	
	//java will call this method to decide how to sort emps
	@Override
	public int compareTo(Employee emp) {
		int val=Integer.compare(this.getId(),emp.getId() );
		return val;
	}
	
	
}
