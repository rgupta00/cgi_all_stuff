package com.day7.session1.collection;
import java.util.*;
public class DemoCollectionEmployee {
	
	public static void main(String[] args) {
		List<Employee> emps=new ArrayList<Employee>();// design to interface in java
		emps.add(new Employee(121, "raja", "sales", 8000));
		emps.add(new Employee(1177, "amit", "it", 8000));
		emps.add(new Employee(1201, "kapil", "sales", 4500));
		emps.add(new Employee(721, "ts", "it", 5700));
		emps.add(new Employee(4421, "abdul", "it", 5900));
		
		System.out.println("---before sorting-------");
		Iterator<Employee> it=emps.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}
		
		//i need to sort the employees
		Collections.sort(emps);
		//u hv create that class, java hv no idea how to sort it
		//u need to help java by imple... an interface
		System.out.println("--- sorting as per id-------");
		it=emps.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}
		//Comparable<T>
		//Comparator<T>
		
		System.out.println("--- sorting as per salary-------");
		//how to force java to use salary sorter?
		Collections.sort(emps, new SalarySorter());
		it=emps.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}
		
		//SortAsPerSalaryThenAsPerName
		
		System.out.println("--- sorting as per salary and then as per name-------");
		//how to force java to use salary sorter?
		Collections.sort(emps, new SortAsPerSalaryThenAsPerName());
		it=emps.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}
		
		
//		for(Employee emp: emps) {
//			System.out.println(emp);
//		}
		
	}

}
