package com.demo.mcq;
interface Movable{
	void move();
}
class Animal{
	public void sound() {
		System.out.println("dont know");
	}
}
class Cat extends Animal{
	public void sound() {
		System.out.println("meou...");
	}
}

class Dog extends Animal implements Movable{
	public void sound() {
		System.out.println("bho bho...");
	}
	public void protectHouse() {
		System.out.println("i dnot take salary but i do my job ....");
	}
	@Override
	public void move() {
		
	}
}
public class UseOfInstanceOf {

	public static void main(String[] args) {
		
		//instanceof operation check parenthood
		
		Dog dog=new Dog();
		if(dog instanceof Movable) {
			System.out.println("yes");
		}else
			System.out.println("no");
		
		
		
//		Animal []animals= {new Dog(), new Dog(), new Cat(), new Cat(), new Dog()};
//		
//		// if it is a normal animal it should do sound if it is dog he should also do protectHouse
//		
//		for(Animal animal : animals) {
//			animal.sound();
//			if(animal instanceof Dog)
//				((Dog) animal).protectHouse();
//		}
//		
	}
	
	
}

