package com.day13.session1;

import java.util.stream.LongStream;

class Prime{
	public static boolean isPrime(Long n){
		boolean isPrimeNumber=true;
		for(int i=2;i<n;i++){
			if(n%i==0)
				isPrimeNumber=false;
		}
		return isPrimeNumber;
	}
}

public class DemoParellelStream {

	public static void main(String[] args) {
		//1 , 100_000
		long start=System.currentTimeMillis();
		
		System.out.println(Runtime.getRuntime().availableProcessors());
		long nos=LongStream.rangeClosed(1, 100_00)
				.filter(Prime::isPrime)
				.parallel()
				.count();
		System.out.println(nos);
		
		long end=System.currentTimeMillis();
		System.out.println("time:"+ (end-start)+" ms");
		
	}
}








