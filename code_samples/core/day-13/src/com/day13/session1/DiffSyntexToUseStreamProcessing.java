package com.day13.session1;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class DiffSyntexToUseStreamProcessing {

	public static void main(String[] args) {
		
		//we want to find all words starts with "a"
		
		List<String> data=List.of("raj","amit","aman");
		
		List<String> names = data.stream().filter(n-> n.startsWith("a"))
		.collect(Collectors.toList());
		
		
		
		//Arrays.strams
		
//		int[]arr= {3,6,7,9,1};
//		
//		IntStream instStrams=Arrays.stream(arr);
//		
//		int value = instStrams.reduce(0, (x,y)-> x+y);
//		
		
		
		//Stream.of
		
		
		//Stream<String> stringSt= Stream.empty();// empty string
		//stringSt.forEach(s-> System.out.println(s));
//		String[] names= {"raj","ekta","gunika","keshav"};
// 		Stream<String> stringSt2=Stream.of(names);
// 		stringSt2.forEach(name-> System.out.println(name));
		
		//list.stream()
		
		// java 9+ List.of(....) : it is a method to create an unmodificatable List
		///List<Integer> list = List.of(3, 5, 6, 7, 83, 9, 11);
		/// list.add(55); it will give the error! as list is unmodificatable

//		Optional<Integer> findAny = list.stream().filter(x-> x%2==0).findAny();
//		
//		System.out.println(findAny.orElseThrow(RuntimeException:: new));

		//Optional<Integer> findAny = list.stream().filter(x -> x % 2 == 0).findAny();
		

		//System.out.println(findAny.orElseThrow(RuntimeException::new));

	}
}
