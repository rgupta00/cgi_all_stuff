package com.demo.exceptions;

public class SomeQuestionOnFinally {
	
	public static void main(String[] args) {
		//never ever throw ex from finally block
		//...........return value from finally block
		
		try{
			int val=callMe();
			System.out.println(val);
		}catch(Exception ex) {
			System.out.println(ex);
		}
	
	}

	private static int callMe() {
		try {
			if(1==1)
				throw new NullPointerException();
		}finally {
			throw new ArithmeticException();// it is very bad code to throw ex from finally block
		}
	}
	
	//...........return value from finally block
//	private static int callMe() {
//		try {
//			if(1==1)
//				return 1;
//		}finally {
//			return 4;
//		}
//	}

}
