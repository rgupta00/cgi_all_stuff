package com.bankapp;

public class AccountTester {

	public static void main(String[] args) {
		try{
			Account account=new Account(121, "raja", 8800);
			account.withdraw(2000);
			account.deposit(30000);
			System.out.println(account);
		}catch(AccountCreationException ex) {
			System.out.println(ex.getMessage());
		}catch(NotSufficientFundException ex) {
			System.out.println(ex.getMessage());
		}catch(OverFundException ex) {
			System.out.println(ex.getMessage());
		}
		
	}
}
