package com.bankapp;

public class AccountCreationException extends Exception{
	public AccountCreationException(String message) {
		super(message);
	}
}
