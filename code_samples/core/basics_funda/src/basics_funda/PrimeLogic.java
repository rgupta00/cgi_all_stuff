package basics_funda;

import java.util.Scanner;

public class PrimeLogic {
	
	public static void main(String[] args) {
		int no;
		Scanner scanner=new Scanner(System.in);
		System.out.println("PE a no");
		no=scanner.nextInt();
		
		boolean isPrime = isPrime(no);
		
		if(isPrime)
			System.out.println("it is a prime no");
		else
			System.out.println("it is not a prime no");
	}

	public  static boolean isPrime(int no) {
		boolean isPrime=true;

		for(int i=2; i<no; i++){
			if(no%i==0){
				isPrime=false;
				break;
			}
		}
		return isPrime;
	}

}
