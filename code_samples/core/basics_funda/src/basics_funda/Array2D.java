package basics_funda;

public class Array2D {
	public static void main(String[] args) {
		int x[][]= {{1,4,7,7,0},{3,5,7,4,77},{12,23,45,0,12},{1,0,2,6,8}};
		
		for(int temp[]: x) {
			for(int val: temp) {
				System.out.print(val+" ");
			}
			System.out.println();
		}
	}
}
