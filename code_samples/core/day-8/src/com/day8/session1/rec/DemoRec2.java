package com.day8.session1.rec;

public class DemoRec2 {

	public static int fact(int n) {
		if(n==1) {
			return 1;
		}else
			return n * fact(n-1);
	}
	public static void main(String[] args) {
		//factorial// 5!
		
		System.out.println(fact(5));
	}
}
