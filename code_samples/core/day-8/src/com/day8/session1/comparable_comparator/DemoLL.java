package com.day8.session1.comparable_comparator;
import java.util.*;

import com.day8.session1.Employee;
public class DemoLL {

	public static void main(String[] args) {
		
		List<Customer> customers=new LinkedList<Customer>();
		customers.add(new Customer(1, "raj", 78, new Date()));
		customers.add(new Customer(2, "amit", 38, new Date()));
		customers.add(new Customer(18, "sumit", 778, new Date()));
		
		print(customers);
		
		Collections.sort(customers);
		System.out.println("-----printing sorted as per id---------");
		print(customers);
		
		System.out.println("-----printing custoer sorted as per purchage capacity---------");
		Collections.sort(customers, new CustomerSortedAsPerPurchageCapacity());
		print(customers);
	}

	private static void print(List<Customer> customers) {
		for(Customer temp: customers) {
			System.out.println(temp);
		}
	}
}
