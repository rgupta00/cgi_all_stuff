package com.day8.session1.comparable_comparator;

import java.util.Date;

public class Customer implements Comparable<Customer> {
	private int id;
	private String name;
	private double puchargeCap;
	private Date dob;

	public Customer(int id, String name, double puchargeCap, Date dob) {
		this.id = id;
		this.name = name;
		this.puchargeCap = puchargeCap;
		this.dob = dob;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", name=" + name + ", puchargeCap=" + puchargeCap + ", dob=" + dob + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPuchargeCap() {
		return puchargeCap;
	}

	public void setPuchargeCap(double puchargeCap) {
		this.puchargeCap = puchargeCap;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	@Override
	public int compareTo(Customer o) {
		return Integer.compare(this.getId(), o.getId());
	}

}
