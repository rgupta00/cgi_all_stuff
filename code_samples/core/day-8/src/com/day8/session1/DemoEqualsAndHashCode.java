package com.day8.session1;
import java.util.*;
/*
 * Compiled from "Object.java"
public class java.lang.Object {
  public java.lang.Object();
  public final native java.lang.Class<?> getClass();
  public native int hashCode();
  public boolean equals(java.lang.Object);
  protected native java.lang.Object clone() throws java.lang.CloneNotSupportedException;
  public java.lang.String toString();
  public final native void notify();
  public final native void notifyAll();
  public final native void wait(long) throws java.lang.InterruptedException;
  public final void wait(long, int) throws java.lang.InterruptedException;
  public final void wait() throws java.lang.InterruptedException;
  protected void finalize() throws java.lang.Throwable;
  static {};
}

 */
public class DemoEqualsAndHashCode {

	public static void main(String[] args) {
		//two employee object
		Object e1=new Employee(121, "RAJ", 48000);
		Object e2=new Employee(121, "RAJ", 48000);
		
//		//dont compare object by there add
//		if(e1==e2) {
//			
//		}
//		
		if(e1.equals(e2)) {
			System.out.println("are eq");
		}else
			System.out.println("not eq");
		
		System.out.println(e1.hashCode());
		System.out.println(e2.hashCode());
		
		HashSet<Employee> set=new HashSet<Employee>();
		set.add(new Employee(121, "AMIT", 30000));
		System.out.println(set.contains(new Employee(121, "AMIT", 30000)));
		
		//contract bw eq and hashcode 
		//if object are not eq there hashcode may be or many not be same
		//if two objects are same there hashcode must be same (imp)
		
		//hahsmap and hashset dont work correctly for user defined object
		
		
	}
}
