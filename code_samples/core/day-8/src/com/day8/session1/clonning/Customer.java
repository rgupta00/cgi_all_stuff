package com.day8.session1.clonning;

import java.util.Date;

public class Customer implements Cloneable{
	private int id;
	private String name;
	private double puchargeCap;
	private Date dob;
	
	public Customer(int id, String name, double puchargeCap, Date dob) {
		this.id = id;
		this.name = name;
		this.puchargeCap = puchargeCap;
		this.dob = dob;
	}
	
	
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		
		//we need to write the code for deep copy
		
		Customer customer= (Customer) super.clone();
		customer.dob=(Date) dob.clone();
		return customer;
	}



	@Override
	public String toString() {
		return "Customer [id=" + id + ", name=" + name + ", puchargeCap=" + puchargeCap + ", dob=" + dob + "]";
	}



	//in morder java is a bad programming practice to rely on finilize method 
	
	//hey java when u are going to call finilize method just before that dont forget to call
	//finalize method
	// we dont know when gc is going to be called
	@Override
	protected void finalize() throws Throwable {
		System.out.println("finalize is called...");
		super.finalize();
	}



	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPuchargeCap() {
		return puchargeCap;
	}
	public void setPuchargeCap(double puchargeCap) {
		this.puchargeCap = puchargeCap;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	
	
}
