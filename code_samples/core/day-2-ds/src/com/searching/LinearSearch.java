package com.searching;

public class LinearSearch {

	public static void main(String[] args) {
		int arr[]= {3,5,6,-3,78,22};
		int pos=-1;
		int val=60;
		boolean isFound=false;
		for(int i=0;i<arr.length; i++) {
			if(arr[i]==val) {
				isFound=true;
				pos=i;
				break;
			}
		}
		if(isFound)
			System.out.println("found: "+ pos);
		else
			System.out.println("not found");
	}
}
