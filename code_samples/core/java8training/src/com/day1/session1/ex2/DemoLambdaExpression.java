package com.day1.session1.ex2;

import java.io.File;
import java.io.FileFilter;

public class DemoLambdaExpression {
	
	public static void main(String[] args) {
		
		//Ann inner class -> lambada expression
		
		//Print  all hidden files
		
		File dir=new File("/opt/sts_eclipse");
		
		if(dir.isDirectory()) {
			File[]files=dir.listFiles(File::isHidden);//method ref is syn suger on lambda expression
			
			//list of files , print them=> java 8
			for(File file: files) {
				System.out.println(file.getName());
			}
			
		}
		
		/*
		 * Lambda expession vs ann inner class
		 * refectoring example => ann inner class==> lambda expression 
		 */
	}

}
