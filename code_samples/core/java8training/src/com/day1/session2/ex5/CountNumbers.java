package com.day1.session2.ex5;
import java.util.*;
import java.util.function.Predicate;
public class CountNumbers {

	public long countEven(List<Long> nos) {
		long count=nos.stream().filter((Long t)->  t%2==0).count();
		return count;
	}
}
