package com.day1.session2.ex7;

import java.util.OptionalLong;
import java.util.function.LongBinaryOperator;
import java.util.function.LongConsumer;
import java.util.stream.LongStream;

public class DemoAccumulationVsReduction {

	public static void main(String[] args) {
		//500000500000
//		long sum=0;
//		for(long i=0;i<=1_000_000;i++){
//			sum=sum+i;
//		}
//		System.out.println(sum);
		
		//problem with accumlation
//		long sum[]=new long[] {0};// why array
//		
//		LongStream.
//		rangeClosed(1, 1_000_000)
//			.parallel()
//			.forEach( i->sum[0]+=i);
		
	//	System.out.println(sum[0]);
		
		
	//Use Reduction:?
		
//		long sum=LongStream
//				.rangeClosed(1, 1_000_000)
//				.parallel()
//				.reduce(0, ( x,  y)-> x+y);
//		System.out.println(sum);
//		
//		long sum=LongStream
//				.rangeClosed(1, 1_000_000)
//				.parallel()
//				.sum();
//		
//		System.out.println(sum);
//		
//		OptionalLong sum=LongStream
//				.rangeClosed(1, 1_000_000)
//				.parallel()
//				.reduce(( x,  y)-> x+y);
//		System.out.println(sum.orElse(0));
		
//		//mutipication of no from 1 to 100
//		long mulitpication =LongStream.rangeClosed
//				(1, 20)
//				.reduce(1, (x,y)-> x*y);
//		
//		System.out.println(mulitpication);
		
	}
}
