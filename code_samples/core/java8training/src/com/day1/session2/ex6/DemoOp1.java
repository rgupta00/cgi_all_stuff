package com.day1.session2.ex6;

import java.util.Optional;
import java.util.function.Supplier;

class EmployeeService{
	
	public Optional<String> getEmployeeById(int id) {
		//db was hit
		return Optional.empty();
	}
}
class NameNotFoundEx extends RuntimeException{}
public class DemoOp1 {
	
	public static void main(String[] args) {
		
		//Supplier is aka factory design pattern *
		
		//Supplier<String> supplier=() -> "supplier supply a string";
	
			Supplier<NameNotFoundEx> exSupplier =()->new NameNotFoundEx();
			
			Supplier<NameNotFoundEx> exSupplier2 = NameNotFoundEx:: new;
			
			//Method ref in details need to be discussed*
			
		
		Optional<String> opString=Optional.ofNullable("raj");
		
//		String name=opString.orElse("name not found");
//		System.out.println(name);
//		
//		String name=opString
//				.orElseThrow(exceptionSupplier)
		
		
//		if(opString.isPresent()) {
//			String val=opString.get();
//			System.out.println(val);
//		}else
//			System.out.println("not found");
		
//		String value=opString.get();//should not use
//		System.out.println(value);
		
		
//		EmployeeService employeeService=new EmployeeService();
//		
//		String name=employeeService.getEmployeeById(322).orElse("employee not found");
//		System.out.println(name);
	}

}
