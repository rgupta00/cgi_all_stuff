package com.day3.session2;
/*
 * a garment factory: Shirt
 * Size
 */

enum ShirtSize{
	S, M, L, XL , XXL;
}
class Shirt{
	private String brand;
	private String color;
	private ShirtSize size;
	public Shirt(String brand, String color, ShirtSize size) {
		super();
		this.brand = brand;
		this.color = color;
		this.size = size;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	
	
	
	
	public ShirtSize getSize() {
		return size;
	}
	public void setSize(ShirtSize size) {
		this.size = size;
	}
	public void printShritDetails() {
		System.out.println("brand:"+ brand);
		System.out.println("size: "+ size);
		System.out.println("color:"+ color);
	}
}
public class DemoEnum {
	
	public static void main(String[] args) {
		Shirt shirt=new Shirt("PE", "light blue", ShirtSize.XL);
		shirt.printShritDetails();
	}

}
