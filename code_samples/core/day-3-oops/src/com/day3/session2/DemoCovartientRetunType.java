package com.day3.session2;
// java 1.5: //Co varient return type?

//advantage of covarient retun type?
//class Milk{
//	
//}
//class PasteurizedMilk extends Milk{
//	
//}
//class KrishnaDairy{
//	public Milk getMilk(double money) {
//		return new Milk();
//	}
//}
//
//class KrishnaDairyImproved extends KrishnaDairy{
//	@Override
//	public PasteurizedMilk getMilk(double money) {
//		return new PasteurizedMilk();
//	}
//}

//class X{
//}
//// Y is co varient of X
//class Y extends X{
//	
//}
//class A{
//	public X foo() {
//		System.out.println("foo method of A");
//		return new X();
//	}
//}
//
//class B extends A{
//	@Override
//	public Y foo() {
//		System.out.println("foo method of B");
//		return new Y();
//	}
//}

public class DemoCovartientRetunType {
	//Co varient return type?
	
	public static void main(String[] args) {
//		A a=new B();
//		a.foo();
	}

}
