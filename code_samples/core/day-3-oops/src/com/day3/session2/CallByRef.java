package com.day3.session2;


/*
 * in java dont support call by refer: when u pass the reference in a method java pass the value of the 
 * ref
 */
class Val{
	private int val;

	public Val() {}

	public Val(int val) {
		this.val = val;
	}

	public int getVal() {
		return val;
	}

	public void setVal(int val) {
		this.val = val;
	}
}
public class CallByRef {
	
	// when u are passing a object in a method call, u can chage the staate of the objet
	//but can not nullify it
	
	public static void changeIt(Val val) {
		val.setVal(500);
	}
	
	
	
	public static void swap(Val a, Val b) {
		Val temp=new Val();
		a=b;
		b=temp;
	}
	
//	public static void swap(int a, int b) {
//		int temp=a;
//		a=b;
//		b=temp;
//	}
	
	public static void main(String[] args) {
		
		Val val=new Val(50);
		System.out.println(val.getVal());
		changeIt(val);
		System.out.println(val.getVal());
		
		
//		Val val1=new Val(5);
//		Val val2=new Val(50);
//		
//		System.out.println(" val1: "+ val1.getVal() + " val2: "+ val2.getVal());
//		swap(val1, val2);
//		System.out.println(" val1: "+ val1.getVal() + " val2: "+ val2.getVal());
	}

}








