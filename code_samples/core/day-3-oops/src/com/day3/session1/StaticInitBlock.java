package com.day3.session1;
//DRY: DONT REPEAT URSELF
class X{
	private int a;
	
	{
		System.out.println("some common code II...");
		
	}
	//init block
	{
		System.out.println("some common code I...");
		System.out.println("some common code.I again..");
	}
	
	//only going to called once at the class loading time (JVM class loader )
	static {
		System.out.println("i am static init block");
	}
	
	
	X(){
		
		System.out.println("i am default ctr");
	}
	X(int a){
		System.out.println("i am para ctr");
	}
}
public class StaticInitBlock {

	public static void main(String[] args) {
		X x=new X(55);
		X x2=new X();
	}
}
