package com.day3.session1;

class Cal{
//	public int add(int a, int b){
//		System.out.println("public int add(int a, int b)");
//		return a+b;
//	}
	
	public int add(int ...a){
		System.out.println("public int add(int ...a)");
		int sum=0;
		for(int i=0;i<a.length; i++) {
			sum+=a[i];
		}
		return sum;
	}
	public Integer add(Integer a, Integer b){
		System.out.println("public Integer add(Integer a, Integer b)");
		return a+b;
	}
}

public class DemoFunctionOverloading {
	
	public static void main(String[] args) {
	
		//variable argument methods 1.5.
		
		Cal cal =new Cal();
		System.out.println(cal.add(2,4));
		
		
		
		
		
		
		
		
		//Boxing/unboxing 1.5
		
		
		
		
		
		
		
//		
//		Integer val=new Integer(33);
//		Integer val2=new Integer(33);
//		
//		Cal cal=new Cal();
//		System.out.println(cal.add(val, val2));
//		
		//Wrapper classes
		
		
//		Integer it=9;
//		it++;
//		System.out.println(it);
		
//		int i=9;
//		Integer it=i;//Autoboxing
//		int temp=it; //auto unboxing
		
		
		
		
//		int i=9;
//		Integer it=new Integer(i);
//		//java 5: autoboxing and unboxing
//		
//		int temp=it.intValue();
//		temp++;
//		it=new Integer(temp);
		
		
	}

}
