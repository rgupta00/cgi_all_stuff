package com.demo;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CalculatorTest {

	private Calculator calculator;
	
	@Before
	public void setUp() throws Exception {
		calculator=new Calculator();
	}
	@Test
	public void addTest() {
		Assert.assertEquals(4, calculator.add(2, 2));
	}
	
	@Test
	public void mulitipationTest() {
		Assert.assertEquals(10, calculator.mul(2, 5));
	}
	
	@Test
	public void divideTest() {
		Assert.assertEquals(4, calculator.divide(20, 5));
	}
	
	@Test(expected = ArithmeticException.class)
	public void divideTestWithArithmaticException() {
		Assert.assertEquals(4, calculator.divide(20, 0));
	}
	
	@After
	public void tearDown() throws Exception {
		calculator=null;
	}
}
