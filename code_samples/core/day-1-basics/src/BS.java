import java.util.Arrays;

public class BS {

	public static void main(String[] args) {
		int x[] = { 3, 5, 6, 7, 8, 90, -1 };
		// sorting :
		Arrays.sort(x);
		printArr(x);
		System.out.println();
		System.out.println("=================");
		int index= Arrays.binarySearch(x, 60);//-7+1=-6
		System.out.println(index);
		
		//serachNoUsingBS(x, 0 , x.length-1, 80);
		

	}

	private static void printArr(int[] x) {
		for (int temp : x) {
			System.out.print(temp+" ");
		}
	}

	public static  void serachNoUsingBS(int x[], int first, int last, int key) {

		int mid = (first + last) / 2;

		// ctr +shift +f
		while (first <= last) {
			if (x[mid] < key) {
				first = mid + 1;
			} else if (x[mid] == key) {
				break;
			} else {
				last = mid - 1;
			}
			mid = (first + last) / 2;
		}

		if (first > last)
			System.out.println("no is not found");
		else
			System.out.println("no is found : " + mid);
	}
}
