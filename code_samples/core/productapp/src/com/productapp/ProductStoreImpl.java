package com.productapp;
import java.io.*;
import java.util.*;
public class ProductStoreImpl implements ProductStore {
	private String storeName;
	private List<Product> products;
	
	public ProductStoreImpl(String name) {
		storeName=name;
		products=new ArrayList<Product>();
		
		//logic to read the file and put all product to the products arrayList
		try{
			BufferedReader br=new BufferedReader(new FileReader(new File("products.txt")));
			String line=null;
			Product tempProduct=null;
			//i have initilize an arraylist from a file
			while((line=br.readLine())!=null) {
				String tokens[]=line.split(",");
				//System.out.println(tokens[0]+":"+ tokens[1]+": "+ tokens[2]+": "+ tokens[3]);
				
				 tempProduct=new Product (Integer.parseInt(tokens[0]), tokens[1],
				  Double.parseDouble(tokens[2]), Integer.parseInt(tokens[3]));
				  products.add(tempProduct);
				 
			}
		}catch(FileNotFoundException ex) {
			ex.printStackTrace();
		}catch(IOException ex) {
			ex.printStackTrace();
		}
	}
	
	@Override
	public void printProductDetails() {
		System.out.println("store name: "+ storeName);
		printProducts();
	}
	
	@Override
	public void printSortedProductsAsPerId() {
		Collections.sort(products);
		printProducts();	
	}
	
	@Override
	public void printSortedProductsAsPerPrice() {
		//?
		Collections.sort(products, new ProductSorterAsPerPrice());
		printProducts();	
	}
	
	private void printProducts() {
		for(Product product: products) {
			System.out.println(product);
		}
	}
}






