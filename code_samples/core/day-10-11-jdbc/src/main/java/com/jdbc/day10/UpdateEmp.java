package com.jdbc.day10;
import java.sql.*;
public class UpdateEmp {

	public static void main(String[] args) {
		// 1. load the driver, dynamic class loading?
				try {
					Class.forName("com.mysql.jdbc.Driver");
					System.out.println("driver is loaded");
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
				//ARM : automatic resorce mgt
				try (Connection connection = 
						DriverManager.getConnection
						("jdbc:mysql://localhost:3306/cgi_demo?useSSL=false","root", "root")) {
					PreparedStatement pstmt=connection.prepareStatement
							("update emp2 set salary=? where id=?");
					pstmt.setInt(2, 1);
					pstmt.setInt(1, 65);
					
					int noOfRowsEffected= pstmt.executeUpdate();
					System.out.println(noOfRowsEffected);
					
					
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
	}
}
