package com.day11.session2;

import java.util.Comparator;

import com.day11.session1.Employee;

interface Foof{
	void foof(String name);
}

interface Greeting{
	public String sayHello(String name);
}
interface Cal{
	public int add(int a, int b) ;
}
public class DemoLambda {
	
	public static void main(String[] args) {
		
		Foof foof= name-> System.out.println(name);
		foof.foof("syed");
		
			
		
		
		
		Greeting greeting = name ->  "hello "+ name;
		
		System.out.println(greeting.sayHello("raj"));
		
		
		Cal cal=( a,  b)->  a+b;
		
		System.out.println(cal.add(4, 3));
		
		
		
		
		
		
//		Comparator<Employee> comparator=( o1,  o2) ->
//				 Integer.compare(o2.getSalary(), o1.getSalary());
			
		
	
//		Runnable runnable=()-> 
//				System.out.println("job ie done by thread....");
//			
//		
//		Thread thread=new Thread(runnable);
//		
	}

}
