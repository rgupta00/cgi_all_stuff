package com.day11.session1;
import java.io.*;
public class DemoLambda2 {
	public static void main(String[] args) {
		
		//more ex where u can use lambda
		//print all hidden files from dir
		
		File dir=new File("/opt/sts_eclipse");
		if(dir.isDirectory()) {
			File[] files=dir.listFiles(File::isHidden);
			
			for(File file: files) {
				System.out.println(file.getName());
			}
		}
		
		
//		File dir=new File("/opt/sts_eclipse");
//		if(dir.isDirectory()) {
//			File[] files=dir.listFiles((fileName) ->fileName.isHidden());
//			
//			for(File file: files) {
//				System.out.println(file.getName());
//			}
//		}
		
		
		
		
		
//		File dir=new File("/opt/sts_eclipse");
//		if(dir.isDirectory()) {
//			File[] files=dir.listFiles(new FileFilter() {
//				
//				@Override
//				public boolean accept(File fileName) {
//					return fileName.isHidden();
//				}
//			});
//			
//			for(File file: files) {
//				System.out.println(file.getName());
//			}
//		}
	}

}
