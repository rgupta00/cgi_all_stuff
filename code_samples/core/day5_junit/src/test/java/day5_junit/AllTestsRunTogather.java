package day5_junit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CalculatorTest.class, 
	CalculatorTest2.class, 
	SlowApiTest.class })
public class AllTestsRunTogather {

}
