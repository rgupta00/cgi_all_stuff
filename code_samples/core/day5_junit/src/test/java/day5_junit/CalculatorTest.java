package day5_junit;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CalculatorTest {

	private Calculator calculator;
	
	@Before
	public void setUpCalculator() {
		calculator =new Calculator();
	}
	
	@Test
	public void addTest() {
		int sum=calculator.add(3,2);
		Assert.assertEquals(5, sum);
		
	}
	
	@Test
	public void divisionTest() {
		int div=calculator.divide(12,2);
		Assert.assertEquals(6, div);
	}
	
	@Test(expected = ArithmeticException.class)
	public void divisionTestFailedwithArithmaticException() {
		int div=calculator.divide(12,0);
		Assert.assertEquals(6, div);
	}
	
	
	@Test
	public void mulTest() {
		int mul=calculator.mul(3,2);
		Assert.assertEquals(6, mul);
	}
	
	@After
	public void tearDownCalculator() {
		calculator =null;
	}
}
