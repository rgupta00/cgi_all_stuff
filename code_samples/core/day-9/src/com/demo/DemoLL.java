package com.demo;


class LinkList{
	class Node{
		int info;
		Node next;
		public Node(int info) {
			this.info = info;
			this.next=null;
		}
	}
	Node first=null, last=null;
	
	public void insert(int data) {
		Node newNode=new Node(data);
		if(first==null) {
			first=last=newNode;
		}else {
			last.next=newNode;
			last=newNode;
		}
	}
	
	public void delLast() {
		if(first==null) {
			System.out.println("no node to delete");
		}else if(first.next==null) {
			Node nodeToDel=first;
			first=last=null;
			System.out.println("deleted:"+nodeToDel.info);
		}else {
			Node prev=null, curr=first;
			while(curr.next!=null) {
				prev=curr;
				curr=curr.next;
			}
			prev.next=null;
		}
	}
	
	
	public void delFirst() {
		if(first==null) {
			System.out.println("no node to delete");
		}else if(first.next==null) {
			Node nodeToDel=first;
			first=last=null;
			System.out.println("deleted:"+nodeToDel.info);
		}else {
			Node nodeToDel=first;
			System.out.println("deleted"+nodeToDel.info);
			first=first.next;
		}
	}
	
	public void insertFirst(int data) {
		Node newNode=new Node(data);
		if(first==null) {
			first=last=newNode;
		}else {
			newNode.next=first;
			first=newNode;
		}
	}
	
	public void insertLast(int data) {
		Node newNode=new Node(data);
		if(first==null) {
			first=last=newNode;
		}else {
			last.next=newNode;
			last=newNode;
		}
	}
	
	public void print() {
		if(first==null) {
			System.out.println("no node to show");
		}else {
			Node temp=first;
			while(temp!=null) {
				System.out.print(temp.info+" ");
				temp=temp.next;
			}
		}
	}
}
public class DemoLL {

	public static void main(String[] args) {
		LinkList ll=new LinkList();
		ll.insert(33);
		ll.insert(3);
		ll.insert(93);
		ll.insert(8);
		ll.insert(5);
		
		ll.delLast();
		ll.print();
	}
}
