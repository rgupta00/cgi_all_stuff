package com.day9.session1.innerclasses;
//when i should go for top level inner class
//when Inner class can not exist without existance of outer classes
//inner and outer class have mother and child relationship
class DebitCard{
	
	public void checkPin(String no) {
		
		class CheckPinLogic{
			private int i;
			
			public boolean doValidation(String no) {
				return true;
			}
			
		}
	}
}
class Outer{
	 private int i=44;
	 
	 class Inner{
		 private int i=55;
		 
		 public void doInner() {
			 System.out.println(Outer.this.i);
			 System.out.println(this.i);
		 }
	 }
	 
	 public  void doOuter() {
		 Inner inner=new Inner();
		 inner.doInner();
	 }
	 
	 //instance method : it should be called with the help of an object of outer ie become this
//	 public static void doOuter() {
//		 Inner inner=new Inner();
//		 inner.doInner();
//	 }
}
public class DemoInnerClasses {

	public static void main(String[] args) {
//		Outer.Inner inner=new Outer().new Inner();
//		inner.doInner();
//		
		
		Outer outer=new Outer();
		outer.doOuter();
		
		
		/*
		 * 			Nested classes
		 * 
		 * 				|
		 *		________________________
		 *		|						|
		 *	 innner class			static inner class (helper classes)
		 *    |
		 *  ---------------
		 *  |						|		|
		 *  top leve		method local 	annoymous inner class
		 *  inner class
		 * 
		 * 
		 * 
		 */
	}
}
