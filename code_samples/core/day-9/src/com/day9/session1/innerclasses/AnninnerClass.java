package com.day9.session1.innerclasses;
interface Cookable{
	public void cook();
}
//class CookableImp implements Cookable{
//	@Override
//	public void cook() {
//		System.out.println("cookable is impl");
//	}
//}
public class AnninnerClass {

	public static void main(String[] args) {
		
		Cookable cookable=new Cookable() {
			@Override
			public void cook() {
				System.out.println("cookable is implemented");
			}
		};
		Cookable cookable2=new Cookable() {
			@Override
			public void cook() {
				System.out.println("cookable 2 is implemented");
			}
		};
		
		Cookable cookable3=new Cookable() {
			@Override
			public void cook() {
				System.out.println("cookable 3 is implemented");
			}
		};
		
	}
}

