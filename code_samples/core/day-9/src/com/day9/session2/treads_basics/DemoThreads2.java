package com.day9.session2.treads_basics;

class Job1 implements Runnable {

	@Override
	public void run() {
		System.out.println("job started by:" + Thread.currentThread().getName());
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("job ended by:" + Thread.currentThread().getName());
	}

}

public class DemoThreads2 {

	public static void main(String[] args) {
		System.out.println("i ma inside main");

		Job1 job = new Job1();// job

		Thread thread1 = new Thread(job, "A");
		Thread thread2 = new Thread(job, "B");
		Thread thread3 = new Thread(job, "C");

		thread1.start();
		
		thread2.start();
		thread3.start();
		//if u want to ensure main should finish at last
		//?
		try {
			thread1.join();// thread1 is saying to main hey join after m
							// main can not finish before thread1
			thread2.join();
			thread3.join(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println(" main is finished");
		
		

	}
}
