package com.bankapp;

public class OverFundException extends Exception{
	public OverFundException(String message) {
		super(message);
	}
}