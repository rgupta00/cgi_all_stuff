package com.day12.session1;

import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;

public class FI_Predicate {
	
	public static void main(String[] args) {
		/*
		 * 1. predicate
		 * 2. funcation
		 * 3. consumer
		 * 4. Supplier
		 */
		
		//Funcation : it convert data from one formate to another formate
		
		//Employee --> name of emp
		Function<Employee, String>empToNameMapper= e->  e.getName();
		
		Function<Employee, Integer>empToSalaryMapper= emp-> emp.getSalary();
		
		System.out.println(empToNameMapper.apply(new Employee(12, "raj", 56, 43)));
		
		// "rajesh"----> 6
		Function<String, Integer>function1=name-> name.length();
		System.out.println(function1.apply("rajesh"));
		
		//Me:W 100		Wife: W 110
		BiFunction<Employee, Employee, Integer> mapper=( e1,  e2)->  e1.getSalary()+ e2.getSalary();
		
		
	}

}
