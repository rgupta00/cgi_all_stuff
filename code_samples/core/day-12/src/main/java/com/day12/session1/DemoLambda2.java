package com.day12.session1;

import java.io.*;

class Demo{
	private int i=44;
	void foo() {
		Runnable r=()-> { //lamabd dont have its own this
				int i=66;
				System.out.println(this.i);
			
		};
		r.run();
	}
}


//class Demo{
//	private int i=44;
//	void foo() {
//		Runnable r=new Runnable() {
//			private int i=55;
//			@Override
//			public void run() {
//				System.out.println(Demo.this.i);
//			}
//		};
//		r.run();
//	}
//}
public class DemoLambda2 {
	public static void main(String[] args) {

		//diff bw lambda and ann inner class
		
		//ann inner class have its own scope but lambda ex have "laxical" scope
		
		Demo d=new Demo();
		d.foo();
		
		
		Runnable r=new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				
			}
		};
		// lambda express vs ann inner class

		
		
		Runnable runnable = () ->System.out.println("job1");
		
		Runnable runnable2 = () ->System.out.println("job1");
		Runnable runnable3 = () ->System.out.println("job1");
		
		
		Thread thread=new Thread(() ->System.out.println("job of thread"));
		
		
		// more ex where u can use lambda
		// print all hidden files from dir

		
		
		
		File dir = new File("/opt/sts_eclipse");
	}

}
