package com.demo.standup;

public class DuplicateFormArray {

	public static void main(String[] args) {
		int a[]= {1,2,4,5,5,6,7};
		//find duplicate
		for(int i=0;i<a.length;i++) {
			for(int j=i+1; j<a.length; j++) {
				if(a[i]==a[j])
					System.out.println("duplicate found: "+ a[i]);
			}
		}
	}
}
