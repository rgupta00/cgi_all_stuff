package com.demo.standup;

import java.util.Scanner;

public class DateProblem {

	public static void main(String[] args) {
		//1. to write logic to check for leap year
		
		//2. how many days are there in a month
		 int dd, mm, yyyy;
		 System.out.println("PE dd part");
		 Scanner scanner=new Scanner(System.in);
		  dd=scanner.nextInt();
		 
		 System.out.println("PE mm part");
		  mm=scanner.nextInt();
		 
		 System.out.println("PE yyyy part");
		
		  yyyy=scanner.nextInt();
		
		  System.out.println("date : "+ dd+" : "+ mm +" : "+ yyyy);
		  
		 dd=dd+1;
		 
		 System.out.println(noOfDaysInGivenMonth(2, 2020) );

		 if(dd> noOfDaysInGivenMonth(mm, yyyy)) {
			 dd=1;
			 mm++;
			 if(mm> 12) {
				 mm=1;
				 yyyy++;
			 }
		 }
	
		 System.out.println("date : "+ dd+" : "+ mm +" : "+ yyyy);
	}
	
	public static int noOfDaysInGivenMonth(int monthNumber, int year) {
		int noOfDays[]= {-1, 31,28,31,30,31,30,31,31,30,31,30,31 };
		if(isLeapYear(year)) {
			noOfDays[2]=29;//as year is leap year days in feb should be 29
		}
		return noOfDays[monthNumber];
	}
	public static boolean isLeapYear(int year) {
		return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
	}
}
